var Rsync = require('rsync');
var path = require('path');

const dir = '/home/staticfiles/www/lottie-biohause';

var rsync = new Rsync()
  .shell('ssh')
  .flags('az')
  .chmod('Du=rwx,Dgo=rx,Fu=rw,Fog=r')
  .source(path.resolve(__dirname, 'dist/*'))
  .destination(`staticfiles@5.9.155.41:${dir}`);

rsync.execute(function (error, code, cmd) {
  console.log('on execute')
});
console.log(rsync.command())