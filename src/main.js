import Lottie from 'lottie-web';
import ScrollMagic from 'scrollmagic';

import innerData from './data.json';
//или
// let outerData = 'https://labs.nearpod.com/bodymovin/demo/markus/isometric/markus2.json';
//указываем откуда данные
let useOuterData = false;

var anim = Lottie.loadAnimation({
		wrapper: document.getElementById('svgContainer'),
		animType: 'canvas', //'canvas'
		loop: true,
		// path: outerData,									//путь до внешних данных
		animationData: innerData,			//если есть данные локально
});
if (useOuterData) {
	anim.addEventListener('config_ready', init);
} else {
	init();
}

function init() {
		var playBtn = document.getElementById('play');
		var pauseBtn = document.getElementById('pause');
		var rangeInput = document.getElementById('rangeInput');
		var onScrollBtn = document.getElementById('onScroll');
		var isPause = false;
		var isDrag = false;
		var onScrollPauseState = false;

		anim.addEventListener('enterFrame', function (e) {
				if (isDrag) 
						return;
				let progress = e.currentTime / e.totalTime;
				rangeInput.value = progress * 100;

		});
		playBtn.addEventListener('click', function () {
				if (onScrollBtn.checked) 
						return;
				anim.play();
		})
		pauseBtn.addEventListener('click', function () {
				if (onScrollBtn.checked) 
						return;
				anim.pause();
		});
		rangeInput.addEventListener('input', function () {
				if (onScrollBtn.checked) 
						return;
				let frame = parseInt(rangeInput.value / 100 * (anim.totalFrames - 1))
				anim.goToAndStop(frame, true);
		});
		rangeInput.addEventListener('mousedown', function () {
				if (onScrollBtn.checked) 
						return;
				isDrag = true;
				isPause = anim.isPaused;
		})
		rangeInput.addEventListener('mouseup', function () {
				if (onScrollBtn.checked) 
						return;
				isDrag = false;
				if (!isPause) {
						anim.play();
				}
		});

		onScrollBtn.addEventListener('input', function (e) {
				if (onScrollBtn.checked) {
						onScrollPauseState = anim.isPaused;
						anim.pause();

						let frame = parseInt(scene.progress() * (anim.totalFrames - 1));
						anim.goToAndStop(frame);
				} else {
						if (!onScrollPauseState) {
								anim.play();
						}
				}
		})
		let trigger = document.querySelector('.bg');
		let controller = new ScrollMagic.Controller();
		let scene = new ScrollMagic
				.Scene({triggerElement: trigger, triggerHook: 0, duration: getDuration(trigger)})
				.on("progress", function (event) {
						if (!onScrollBtn.checked) 
								return;
						let frame = parseInt(event.progress * (anim.totalFrames - 1));
						anim.goToAndStop(frame, true)
				})
				.addTo(controller);
		window.addEventListener('resize', function () {
				scene.duration(getDuration(trigger));
		})
}

function getDuration(trigger) {
		return trigger.offsetHeight - window.innerHeight;
}